import numpy as np
import pandas as pd
from tree import DecisionTree
from typing import List
import timeit
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score
import matplotlib.pyplot as plt

#### For part 2 of the assignment: ####


def homog_data(items):
    """
    Checks if all values in given input array "items" are identical.
    """
    return all(x == items[0] for x in items)


def time_function(input_function):
    """
    Runs and times given function.
    """
    start = timeit.timeit()
    input_function
    end = timeit.timeit()
    print("Time elapsed: ", end - start)


def impurity(y):
    """
    :param y: target var
    :return: gini reduction for node given potential split
    """
    # Compute relative frequency of each class label in vector
    (unique, counts) = np.unique(y, return_counts=True)
    sum_counts = sum(counts)
    # Option 1:
    #probs = [counts[label]/sum(counts) for label in range(len(unique))]
    #return 1 - np.sum(np.square(probs))  # gini index

    # Option 2:
    probs = [counts[label] / sum_counts for label in range(len(unique))]
    gini = [p * (1-p) for p in probs]
    return np.sum(gini)


def bestsplit(x, y, minleaf):
    """
    :param x: column to be split on
    :param y: target variable
    :return: best split
    """
    if len(x) != len(y):
        print("Length of input x ({0}) not equal to length input y ({1}).".format(len(x), len(y)))
    segments = np.sort(np.unique(x))
    pot_splits = (segments[0:-1]+segments[1:])/2  # Potential split values
    overall_count = len(x)  # Overall number of data points
    overall_impurity = impurity(y)  # Compute initial impurity
    impurity_reduction = 0  # Initialize to 0
    threshold = 0  # Initialize to 0

    for i, pot_split in enumerate(pot_splits):
        # Sort input x
        sorted_ = np.argsort(x)

        # Sort x and y based on sorted_
        x_sorted = x[sorted_]
        y_sorted = y[sorted_]

        # Create mask to split on potential split threshold
        mask = x_sorted > pot_split

        # Split in left and right node, based on threshold
        left_node = x_sorted[mask]
        right_node = x_sorted[~mask]

        if len(left_node) < minleaf or len(right_node) < minleaf:
                #print("Less data than minleaf in either leaf")
                continue  # Go to next pot split item in pot_splits
        else:
            if len(pot_splits) == 1:
                left_labels = y_sorted[mask]
                right_labels = y_sorted[~mask]

                left_gini = impurity(left_labels.astype(int))
                right_gini = impurity(right_labels.astype(int))
                weighted_gini = (len(left_node) / overall_count) * left_gini + (
                            len(right_node) / overall_count) * right_gini
                gain = overall_impurity-weighted_gini
                return pot_split, gain

            left_labels = y_sorted[mask]
            right_labels = y_sorted[~mask]

            left_gini = impurity(left_labels)
            right_gini = impurity(right_labels)
            weighted_gini = (len(left_node)/overall_count) * left_gini + (len(right_node)/overall_count) * right_gini
            gain = overall_impurity - weighted_gini

            if gain > impurity_reduction:
                impurity_reduction = gain  # Best impurity reduction from split in this column
                threshold = pot_split  # Best value to split this column on
    return threshold, impurity_reduction


def col_splits(x: np.asarray, y: np.asarray, minleaf: int):
    """
    Computes best split in each column and relevant gini index.
    :param x: features to be split on
    :param y: target variable
    :return: tuple of: col to split on, the threshold value and actual impurity value
    """
    threshold = None  # Initialize to 0
    best_gini = 0  # Initialize best gini reduction to 0
    index_ = None  # Initialize column index to 0
    for col in range(x.shape[1]):
        col_data = x[:, col]
        split_value, gini = bestsplit(col_data, y, minleaf)
        if gini >= best_gini:
            index_ = col
            threshold = split_value
            best_gini = gini
    return index_, threshold, best_gini  # Column index, split threshold, gini reduction value


def tree_grow(x: np.asarray, y: np.asarray, nmin: int, minleaf: int,
              nfeat: int) -> DecisionTree:
    tree_obj = DecisionTree()
    tree_obj.tree_grow(x, y, nmin=nmin, minleaf=minleaf, nfeat=nfeat)
    return tree_obj


def tree_pred(x: np.asarray, tr: DecisionTree) -> np.asarray:
    return tr.tree_predict(x)


def tree_grow_b(x: np.asarray, y: np.asarray, nmin: int, minleaf: int,
              nfeat: int, m: int) -> List[DecisionTree]:
    """
    :param x: input training data. Both features and target labels.
    :param n: draw n bootstrap samples from the training set
    :return: List of grown trees
    """
    # Initialize variables
    tree_list = []  # Used to store single trees

    for i in range(m):  # Construct n trees
        # Step 1. Draw n samples from training set
        idx = np.random.randint(0, high=x.shape[0], size=x.shape[0])
        sample_x = x[idx, :]  # To-do: Get indices from sample and select these rows from x and y
        sample_y = y[idx]
        # Step 2. Construct tree on each of n bootstrap samples.
        temp_tree = DecisionTree()
        temp_tree.tree_grow(sample_x, sample_y, nmin=nmin, minleaf=minleaf, nfeat=nfeat)
        tree_list.append(temp_tree)  # Store tree
    return tree_list


def tree_pred_b(x: np.asarray, tree_list: List[DecisionTree]) -> np.asarray:
    """
    :param x: input training data. Both features and target labels.
    :param tree_list:
    :return:
    """
    # Predict class for a new case, make prediction with each of the n individual trees and select most
    preds = []
    for tree in tree_list:
        if len(preds) == 0:
            preds = tree_pred(x, tree)
        else:
            new_preds = tree_pred(x, tree)
            preds = np.vstack([preds, new_preds])

    # Assign most frequently predicted class. -> round the column wise mean
    final_predictions = np.round(np.mean(preds, axis=0))
    return final_predictions


def part_2_assignment():

    # 41 features + target label (= last column; "post").
    columns = ["pre", "ACD_avg", "ACD_max", "ACD_sum", "FOUT_avg", "FOUT_max", "FOUT_sum", "MLOC_avg", "MLOC_max",
        "MLOC_sum", "NBD_avg", "NBD_max", "NBD_sum", "NOCU", "NOF_avg", "NOF_max", "NOF_sum",
        "NOI_avg", "NOI_max", "NOI_sum", "NOM_avg", "NOM_max", "NOM_sum", "NOT_avg", "NOT_max",
        "NOT_sum", "NSF_avg", "NSF_max", "NSF_sum", "NSM_avg", "NSM_max", "NSM_sum", "PAR_avg",
        "PAR_max", "PAR_sum", "TLOC_avg", "TLOC_max", "TLOC_sum", "VG_avg", "VG_max", "VG_sum", "post"
        ]

    train_data = pd.read_csv('eclipse-metrics-packages-2.0.csv', delimiter=';', usecols=columns, header=0)[columns]
    test_data = pd.read_csv('eclipse-metrics-packages-3.0.csv', delimiter=';', usecols=columns, header=0)[columns]

    train_data = train_data.values
    test_data = test_data.values

    # Split train and test sets
    train_x = train_data[:,:-1]
    test_x = test_data[:,:-1]
    train_y = train_data[:, -1]
    test_y = test_data[:, -1]

    # Y should be: whether or not post-release bugs have been reported

    return train_x, train_y, test_x, test_y


def convert_to_binary(y: np.asarray) -> np.asarray:
    """
    :param y:
    :return: np array where all 0's of input array y are 1 and all values higher than 0 are set to 0
    """
    mask = y < 1
    return mask.astype(int)


def get_first_n_splits(tree: DecisionTree, n: int, track="left"):
    """
    Used to create a picture of the first two splits of the single tree (the split in the root
    node, and the split in its left or right child). Consider the classification
    rule that you get by assigning to the majority class in the three leaf nodes
    of this heavily simplified tree. Discuss whether this classification rule
    makes sense, given the meaning of the attributes.
    :param tree:
    :param n:
    :param track:
    :return:
    """
    cur_layer = tree.get_tree()  # get the tree we build in training
    i = 0
    while cur_layer.get('depth') <= n:
        if i == 0:
            print("Root node:")
        print("Index column: {0}, Cutoff value: {1:.4f}, Majority class: {2}".format(cur_layer.get('index_col'), cur_layer.get('cutoff'), cur_layer.get('val')))
        print("--"*10)
        i += 1
        if not track == "left":
            cur_layer = cur_layer['right']
            if cur_layer.get("depth") <= n:
                print("Right child node: ")

        else:
            cur_layer = cur_layer['left']
            if cur_layer.get("depth") <= n:
                print("Left child node: ")


def plot_confusion_matrix(cm, y_labels, title='Confusion matrix', cmap=plt.cm.Blues):
    fig, ax = plt.subplots(figsize=(10,10))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title, y=1.045)
    plt.grid(visible=False)
    plt.tight_layout()

    tick_marks = np.arange(len(y_labels))

    plt.xticks(tick_marks, y_labels, rotation=0)
    plt.yticks(tick_marks, y_labels)

    ax.set_ylabel('True label')
    ax.set_xlabel('Predicted label')

    ax.xaxis.set_ticks_position('top')
    ax.xaxis.set_label_position('top')

    # Loop over data dimensions and create text annotations.
    fmt = '.2f'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            plt.text(j, i, format(cm[i, j], fmt),
                     ha="center", va="center",
                     color="white" if cm[i, j] > thresh else "black")


def print_stats(y_test, y_pred, filename, normalize=False):
    """Function that gives some information on overall
    and label-wise performance of the model."""
    cm = confusion_matrix(y_test, y_pred)
    print(cm)
    plt.figure(figsize=(10, 10))
    plt.tight_layout()
    plot_confusion_matrix(cm, y_labels=np.unique(y_test))
    plt.savefig(filename)
    if normalize:
        # Normalize the confusion matrix by row (i.e by the number of samples
        # in each class)
        cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        plt.figure(figsize=(10, 10))
        plt.tight_layout()
        plot_confusion_matrix(cm_normalized, title='Normalized Confusion Matrix')
        plt.savefig("Normalised" + filename)


def evaluate(y, y_hat, filename="tree_conf_matrix.jpg"):
    """
    Prints accuracy, precision and recall scores for given target data and predicted values.
    :param filename:
    :param y:
    :param y_hat:
    :return:
    """
    print(confusion_matrix(y, y_hat))

    # accuracy: (tp + tn) / (p + n)
    accuracy = accuracy_score(y, y_hat)
    print('Accuracy: %f' % accuracy)
    # precision tp / (tp + fp) https://scikit-learn.org/stable/modules/generated/sklearn.metrics.precision_score.html
    precision = precision_score(y, y_hat)
    print('Precision: %f' % precision)
    # recall: tp / (tp + fn) https://scikit-learn.org/stable/modules/generated/sklearn.metrics.recall_score.html
    recall = recall_score(y, y_hat)
    print('Recall: %f' % recall)
    # Create and store confusion matrix figure
    print_stats(y, y_hat, filename)


train_x, train_y, test_x, test_y = part_2_assignment()

# Convert class labels to binary: either post-release bugs or no post-release bugs.
train_y = convert_to_binary(train_y)
test_y = convert_to_binary(test_y)

print("Train shape: ", train_x.shape)
print("Test shape: ", test_x.shape)

### Grow single tree ###
grow_tree = True

if grow_tree: # Set to true if you want to run the single tree example
    print("Growing tree.")
    # Train single tree on train data
    tree_obj = tree_grow(train_x, train_y, nmin=15, minleaf=5, nfeat=41)

    # Predict on test set
    tree_preds = tree_pred(test_x, tree_obj)

    # Evaluate single tree
    evaluate(test_y, tree_preds, filename="single_tree_conf_matrix.jpg")
    print("Done!")

# Print root node info and first split info
get_first_n_splits(tree_obj, n=2, track='right')

columns = ["pre", "ACD_avg", "ACD_max", "ACD_sum", "FOUT_avg", "FOUT_max", "FOUT_sum", "MLOC_avg", "MLOC_max",
           "MLOC_sum", "NBD_avg", "NBD_max", "NBD_sum", "NOCU", "NOF_avg", "NOF_max", "NOF_sum",
           "NOI_avg", "NOI_max", "NOI_sum", "NOM_avg", "NOM_max", "NOM_sum", "NOT_avg", "NOT_max",
           "NOT_sum", "NSF_avg", "NSF_max", "NSF_sum", "NSM_avg", "NSM_max", "NSM_sum", "PAR_avg",
           "PAR_max", "PAR_sum", "TLOC_avg", "TLOC_max", "TLOC_sum", "VG_avg", "VG_max", "VG_sum", "post"
           ]

print(columns[0])
print(columns[39])

### Bagging ###
bagging = True  # Set to true if you want to run the random forest example

if bagging:
    print("Using bagging.")
    bagging = tree_grow_b(train_x, train_y, nmin=15, minleaf=5, nfeat=41, m=100)
    print("Predicting test values using bagging.")
    bagging_pred = tree_pred_b(test_x, bagging)

    # Evaluate forest
    evaluate(test_y, bagging_pred, filename="bagging_conf_matrix.jpg")
    print("Done!")



### Grow forest ###
grow_forest = True  # Set to true if you want to run the random forest example

if grow_forest:
    print("Growing forest.")
    forest = tree_grow_b(train_x, train_y, nmin=15, minleaf=5, nfeat=6, m=100)
    print("Predicting test values using forest.")
    forest_pred = tree_pred_b(test_x, forest)

    # Evaluate forest
    evaluate(test_y, forest_pred, filename="forest_conf_matrix.jpg")
    print("Done!")


### Pima data ###
pima_data = np.genfromtxt('pima_data.txt', delimiter=',', skip_header=True)


def pima_test():
    print("Test PIMA")
    pima_x = pima_data[:, :-1]
    pima_y = pima_data[:, -1]

    #test_obj2 = tree_grow_b(pima_x, pima_y, nmin=20, minleaf=5, nfeat=3, m=5)
    #pima_preds = tree_pred_b(pima_x, test_obj2)

    test_obj2 = tree_grow(pima_x, pima_y, nmin=20, minleaf=5, nfeat=8)
    pima_preds = tree_pred(pima_x, test_obj2)
    print("Done!")

    evaluate(pima_y, pima_preds, filename="pima_conf_matrix.jpg")


test_pima = False  # Set to true if you want to run the pima dataset example
if test_pima:
    pima_test()



