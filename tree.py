# -*- coding: utf-8 -*-
"""
Ijsbrand Daleboudt: 6212573
Joost Gadellaa: 5609704
Yde Jansen: 4116232
"""

import numpy as np
import timeit
from pprint import pprint
from typing import List
from sklearn.metrics import confusion_matrix, accuracy_score


def homog_data(items):
    """
    Checks if all values in given input array "items" are identical.
    """
    return all(x == items[0] for x in items)


def time_function(input_function):
    """
    Runs and times given function.
    """
    start = timeit.timeit()
    input_function
    end = timeit.timeit()
    print("Time elapsed: ", end - start)


def impurity(y):
    """
    This function returns the impurity of a given array y.

    :param y: target var
    :return: gini reduction (float) for node given potential split
    """

    # Compute relative frequency of each class label in vector
    (unique, counts) = np.unique(y, return_counts=True)
    sum_counts = sum(counts)

    probs = [counts[label] / sum_counts for label in range(len(unique))]
    gini = [p * (1-p) for p in probs]

    return np.sum(gini)


def bestsplit(x, y, minleaf):
    """
    This function returns the best split given an array
    
    x and y are both arrays. minleaf is an integer.

    :param x: column to be split on
    :param y: target variable
    :param minleaf: minimum number of observations required for a leaf node

    :return: best split (float) and impurity reduction (float)
    """

    if len(x) != len(y):
        print("Length of input x ({0}) not equal to length input y ({1}).".format(len(x), len(y)))
    segments = np.sort(np.unique(x))

    # Compute potential split values
    pot_splits = (segments[0:-1]+segments[1:])/2  

    # Overall number of data points
    overall_count = len(x)  

    # Compute initial impurity
    overall_impurity = impurity(y)  

    # Initialize threshold and impurity reduction to 0
    impurity_reduction = 0  
    threshold = 0  

    for i, pot_split in enumerate(pot_splits):

        # Sort input x
        sorted_ = np.argsort(x)

        # Sort x and y based on sorted_
        x_sorted = x[sorted_]
        y_sorted = y[sorted_]

        # Create mask to split on potential split threshold
        mask = x_sorted > pot_split

        # Split in left and right node, based on threshold
        left_node = x_sorted[mask]
        right_node = x_sorted[~mask]

        # If left node or right node is smaller than minleaf:
        if len(left_node) < minleaf or len(right_node) < minleaf:
                # Go to next pot split item in pot_splits
                continue  
        else:

            # Check if potential splits is 1
            if len(pot_splits) == 1:
                left_labels = y_sorted[mask]
                right_labels = y_sorted[~mask]

                # Calculate gini for both nodes
                left_gini = impurity(left_labels.astype(int))
                right_gini = impurity(right_labels.astype(int))
                weighted_gini = (len(left_node) / overall_count) * left_gini + (
                            len(right_node) / overall_count) * right_gini
                gain = overall_impurity-weighted_gini
                # Put "if len(pot_splits) == 1:" here, and if True -> return pot_split, gain. Else: go to if gain > imp ..
                return pot_split, gain

            # If the number of potential splits is not 1
            left_labels = y_sorted[mask]
            right_labels = y_sorted[~mask]

            # Calculate gini
            left_gini = impurity(left_labels)
            right_gini = impurity(right_labels)
            weighted_gini = (len(left_node)/overall_count) * left_gini + (len(right_node)/overall_count) * right_gini
            gain = overall_impurity - weighted_gini

            # If the gain is bigger than the impurity reduction
            if gain > impurity_reduction:

                # Save best impurity reduction and value in this column
                impurity_reduction = gain  
                threshold = pot_split  

    return threshold, impurity_reduction


def col_splits(x: np.asarray, y: np.asarray, minleaf: int):
    """
    This function computes the best split in each column and relevant gini index.

    x and y are np.arrays. minleaf is an integer.

    :param x: features to be split on
    :param y: target variable
    :return: tuple of: col to split on, the threshold value and actual impurity value
    """

    # Initialize threshold, gain and index_ to 0
    threshold = None  
    best_gini = 0  
    index_ = None  

    # Loop through the columns
    for col in range(x.shape[1]):
        col_data = x[:, col]
        split_value, gini = bestsplit(col_data, y, minleaf)

        # Check whether computed gini is better
        if gini >= best_gini:
            index_ = col
            threshold = split_value
            best_gini = gini

    # Return column index, split threshold, gini reduction value
    return index_, threshold, best_gini  


class DecisionTree:
    def __init__(self):
        self.depth = 0
        self.max_depth = 30

    def tree_grow(self, x: np.asarray, y: np.asarray, nmin: int, minleaf: int, nfeat: int, parent_node={}, depth=0):
        """
        :param
        x: 2D array of training data
        y: 1D array with target class labels
        nmin: number of observations that a node must contain at least, for it to be allowed to
        be split. In other words: if a node contains fewer cases than nmin, it becomes a
        leaf node.
        minleaf: minimum number of observations required for a leaf node
        nfeat: Every time we compute the best split in a particular node, we first draw at random nfeat features
        from which the best split is to be selected. For "normal" tree growing, nfeat is equal to the total
        number of predictors (the number of columns of x). For random forests, nfeat
        is smaller than the total number of predictors.
        :return:
        """
        if parent_node is None:
            #print("Tree stopped at previous level.")
            return None

        elif len(y) == 0:
            #print("Len(y) = 0.")
            return None

        elif homog_data(y):  # => create leaf node for this data
            #print("Homogeneous data: ", y)
            return {'val': y[0]}

        elif depth >= self.max_depth:
            #print("Reached max depth.")
            return None

        else:

            # Her: first check whether len(y) > minleaf 
            col_index, threshold, gini_index = col_splits(x, y, minleaf)

            # Create child nodes based on class labels using best found split (column and threshold)
            left_child = y[x[:, col_index] > threshold]  # left hand side data
            right_child = y[x[:, col_index] <= threshold]  # right hand side data

            # Now: if left_child + right_child combined is smaller than nmin
            # and if both childs are larger than minleaf (already tested in the col_splits function???)
            # then create parent and child nodes, else: create just a node


            # If the total number of integers in the node is the same or bigger than nmin
            if (left_child.shape[0]+right_child.shape[0]) >= nmin:

                # Create a new node
                parent_node = {'index_col': col_index, 'cutoff': threshold,
                               'val': np.round(np.mean(y)), 'depth': depth+1}  # store data

                # Grow tree for left and right side data
                # If both leafs won't be big enough: parent node becomes a leaf node.
                if len(left_child) >= minleaf and len(right_child) >= minleaf:
                    parent_node['left'] = self.tree_grow(x[x[:, col_index] > threshold], left_child, nmin=nmin, minleaf=minleaf,
                                                         nfeat=nfeat, parent_node={}, depth=depth+1)

                    parent_node['right'] = self.tree_grow(x[x[:, col_index] <= threshold], right_child, nmin=nmin, minleaf=minleaf,
                                                          nfeat=nfeat, parent_node={}, depth=depth+1)

                # Update tree information
                self.depth += 1
                self.trees = parent_node
                return parent_node

            # If the nmin-criterium is not met
            else:
                
                # Make a leaf node
                parent_node = {'val': np.round(np.mean(y)), 'depth': depth+1}  # Actually leaf node. 

                return parent_node


    def tree_predict(self, x: np.asarray):
        """
        x: 2D array of training data
        tr: tree object (created in and output of tree_grow())

        Output:
        y: 1D vector of predicted class labels for input data x

        :return:
        """
        results = np.asarray([0] * len(x), dtype=np.float64)
        for i, c in enumerate(x):  # for each row in test data
            results[i] = self._get_prediction(c)
        return results

    def _get_prediction(self, row) -> np.float64:
        """

        :param row:
        :return:
        """

        # get the tree we build in training
        cur_layer = self.trees  

        # if not leaf node
        while cur_layer.get('cutoff'):  

            # If for given input data: value in next best split column is larger than threshold
            # go to left child node, else go to right child node.
            if row[cur_layer['index_col']] > cur_layer['cutoff']:  # get the direction
                cur_layer = cur_layer['left']
            else:
                cur_layer = cur_layer['right']
        
        # if leaf node, return value
        else:  
            return cur_layer.get('val')

    def get_tree(self):
        return self.trees


def tree_grow(x: np.asarray, y: np.asarray, nmin: int, minleaf: int,
              nfeat: int) -> DecisionTree:
    """

    :param x:
    :param y:
    :param nmin:
    :param minleaf:
    :param nfeat:
    :return:
    """
    tree_obj = DecisionTree()
    tree_obj.tree_grow(x, y, nmin=nmin, minleaf=minleaf, nfeat=nfeat)
    return tree_obj


def tree_pred(x: np.asarray, tr: DecisionTree) -> np.asarray:
    """

    :param x:
    :param tr:
    :return:
    """
    return tr.tree_predict(x)


def tree_grow_b(x: np.asarray, y: np.asarray, nmin: int, minleaf: int,
              nfeat: int, m: int) -> List[DecisionTree]:
    """
    :param x: input training data. Both features and target labels.
    :param n: draw n bootstrap samples from the training set
    :return: List of grown trees
    """

    # Initialize variables
    tree_list = []  # Used to store single trees

    for i in range(m):  # Construct n trees

        # Step 1. Draw n samples from training set
        idx = np.random.randint(0, high=x.shape[0], size=x.shape[0])
        sample_x = x[idx, :]  # To-do: Get indices from sample and select these rows from x and y
        sample_y = y[idx]

        # Step 2. Construct tree on each of n bootstrap samples.
        temp_tree = DecisionTree()
        temp_tree.tree_grow(sample_x, sample_y, nmin=nmin, minleaf=minleaf, nfeat=nfeat)
        tree_list.append(temp_tree)  # Store tree
    return tree_list


def tree_pred_b(x: np.asarray, tree_list: List[DecisionTree]) -> np.asarray:
    """
    :param x: input training data. Both features and target labels.
    :param tree_list:
    :return:
    """
    # Predict class for a new case, make prediction with each of the n individual trees and select most
    preds = []
    for tree in tree_list:
        if len(preds) == 0:
            preds = tree_pred(x, tree)
        else:
            new_preds = tree_pred(x, tree)
            preds = np.vstack([preds, new_preds])

    # Assign most frequently predicted class. -> round the column wise mean
    final_predictions = np.round(np.mean(preds, axis=0))
    return final_predictions


# Simple test on credit data. Use nmin =2, minleaf=1.
def credit_test():
    print("Credit test")
    start = timeit.timeit()
    tree_obj = tree_grow(credit_x, credit_y, nmin=2, minleaf=1, nfeat=5)

    end = timeit.timeit()
    print("Time elapsed: ", end-start)
    return tree_obj


test_credit = False  # Set to true to run the credit data example

if test_credit:
    credit_data = np.genfromtxt('credit.txt', delimiter=',', skip_header=True)
    credit_x = credit_data[:, :5]
    credit_y = credit_data[:, 5]

    test = credit_test()

    test_pred = tree_pred(credit_x, test)
    print(test_pred)
    print(credit_y)

    print("Test_pred[0]: ", type(test_pred[0]))
    print("Credit_y[0]: ", type(credit_y[0]))

