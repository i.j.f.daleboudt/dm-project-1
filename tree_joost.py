#!/usr/bin/env python
# coding: utf-8

# # Part 1: Programming
# The code for this part should be written in R or Python.
# Write a function to grow a classification tree. Also write a function that uses
# this tree to predict the class label for given attribute values. More specifically you should write two main functions, with the names tree_grow and tree_pred. The function tree_grow has input arguments x, y, nmin, minleaf,and nfeat, in that order. Here x is a data matrix (2-dimensional array) containing the attribute values. Each row of x contains the attribute values of one training example. You may assume that all attributes are numeric. y is the vector (1-dimensional array) of class labels. The class label is binary, with values coded as 0 and 1. Furthermore, you may assume there are no missing values (either in training or prediction).
#
# The parameters nmin and minleaf (both integers) are used to stop growing the tree early, to prevent overfitting and/or to save computation. nmin is the number of observations that a node must contain at least, for it to be allowed to be split. In other words: if a node contains fewer cases than nmin, it becomes a leaf node. minleaf is the minimum number of observations required for a leaf node; hence a split that creates a node with fewer than minleaf observations is not acceptable. If the algorithm performs a split, it should be the best split that meets the minleaf constraint. If there is no split that meets the minleaf constraint, the node becomes a leaf node. Use the gini-index for determining the quality of a split. The parameter nfeat denotes the number of features that should be considered for each split. Every time we compute the best split in a particular node, we first draw at random nfeat features from which the best split is to be selected. For “normal” tree growing, nfeat is equal to the total number of predictors (the number of columns of x). For random forests, nfeat is smaller than the total number of predictors.

# *TODO move functions to object where appliccable*
#
# TODO input en output van functies omschrijven

# In[1]:


import random


# In[2]:


class TreeNode():

    def __init__(self, x, y=None):

        self.data= []
        self.condition = None
        self.left = None
        self.right = None

        # If class labels are given in a seperate variable, we add them to the data here
        if y is None:
            self.data = x
        else:
            for i in range(len(y)):
                self.data.append(x[i] + [y[i]])


    def grow(self, nmin=1, minleaf=1, nfeat='All'):

        # Compute current gini
        current_gini = gini_index(self.data)
        # Compute the best split for self
        self.condition = best_split(self.data, nmin, minleaf, nfeat, current_gini)

        # If there is no best split within the constraints we stay a leaf, like instatiated
        if self.condition == None:
            return

        # Else, the node splits itself according to the condition, and recusively calls 'grow' on the child nodes
        else:
            children = make_split(self.data, self.condition[0], self.condition[1])
            self.left = TreeNode(children[0])
            self.right = TreeNode(children[1])
            self.left.grow(nmin, minleaf)
            self.right.grow(nmin, minleaf)
            #print(self.data, '\n gesplitst op condition\n', self.condition, '\nmet als resultaat\n', self.left.data, '\nen\n', self.right.data)





# In[3]:


def gini_index(node):
# Returns the purity for one node

    # Get a list of the class labels
    class_labels = [item[-1] for item in node]

    # Calculate p(1|t)
    p1 = sum(class_labels) / len(class_labels)

    # Return the Gini index (two-class case)
    index = p1 * (1-p1)

    return index


# In[4]:


def weighted_gini_index(nodes):
# Returns the purity for a list of two nodes

    n_node0 = len(nodes[0])
    n_node1 = len(nodes[1])
    n_total = n_node0 + n_node1

    weighted_node0_gini = gini_index(nodes[0]) * n_node0 / n_total
    weighted_node1_gini = gini_index(nodes[1]) * n_node1 / n_total

    weighted_total_gini = weighted_node0_gini + weighted_node1_gini

    return weighted_total_gini


# In[5]:


def candidate_splits(x, attribute_index):
# Returns a list of values on which splits are possible for a given array and attribute index

    # Get the values into a list
    values = [row[attribute_index] for row in x]

    # Remove duplicates
    values = set(values)

    # Sort the list
    values = sorted(values)

    # Calculate halfway points
    split_values = [((values[i]+values[i+1])/2) for i in range(len(values) - 1)]

    return split_values


# In[6]:


def make_split(node, attribute_index, value):
# Splits an array into two arrays on a given attribute index and value

    # Prepare two child nodes
    node1 = []
    node2 = []

    # Iterate over the items in the node and sort them
    for item in node:
        if item[attribute_index] <= value:
            node1.append(item)
        else:
            node2.append(item)

    return [node1, node2]


# In[7]:


def best_split(x, nmin, minleaf, nfeat, current_gini):
# Returns an attribute index and value on which the given array can be best split, considering the given requirements and the current impurity

    # Return None if the nmin requirement is not met or the node consists only one item
    if len(x) < nmin or len(x) == 1:
        #print('No best split for', x, 'because nmin', nmin, 'is not met')
        return None

    lowest_gini = current_gini
    best_attribute = None
    split_value = None

    # Determine which attributes to consider
    consider = [i for i in range(len(x[0]) - 1)]
    if nfeat != 'All':
        consider = random.sample(consider, nfeat)

    # Iterate over the different attributes
    for i in range(len(x[0]) - 1):

        # Get candidate split values
        candidate_splits_v = candidate_splits(x, i)

        # Get candidate splits
        candidate_splits_s = [make_split(x, i, value) for value in candidate_splits_v]

        # Discard all splits that do not meet the required minleaf
        candidate_splits_s_c = []
        for check in candidate_splits_s:
            if (len(check[0]) >= minleaf) and (len(check[1]) >= minleaf):
                candidate_splits_s_c.append(check)

        # Skip the rest if there are no valid splits
        if candidate_splits_s_c == []:
            continue

        # Compute ginis
        candidate_splits_g = [weighted_gini_index(split) for split in candidate_splits_s_c]

        # Get the lowest value
        best = min(candidate_splits_g)

        # And if it is the best until now, set it as best split
        if best < lowest_gini:
            lowest_gini = best
            best_attribute = i
            split_value = candidate_splits_v[candidate_splits_g.index(best)]

        # Return None instead of None, None if the minleaf constraint can't be met
        if best_attribute == None:
            #print('No best split for', x, 'because minleaf', minleaf, 'is not met')
            return None

    return best_attribute, split_value


# The function tree_grow should return a “tree object” that can be used for predicting new cases. You are free to choose the data structure for the tree object, as long as it can be used for predicting new cases in the following way. A new case is dropped down the tree, and assigned to the majority class of the leaf node it ends up in. More precisely, the function tree_pred has input arguments x and tr, in that order. Here x is a data matrix (2-dimensional array) containing the attribute values of the cases for which predictions are required, and tr is a tree object created with the function tree_grow. The function tree_pred has a single output argument y, which is the vector (1-dimensional array) of predicted class labels for the cases in x.

# In[8]:


def tree_grow(x, y=None, nmin=0, minleaf=0, nfeat='All'):

    tr = TreeNode(x, y)
    tr.grow(nmin, minleaf, nfeat)
    return tr


# In[9]:


def tree_pred(x, tr):

    y = []
    for item in x:

        # Set the current classifying node to the top of the tree
        current_node = tr

        # While we are not at a leaf node
        while current_node.condition != None:

            # We get the next tree based on the split condition
            if x[current_node.condition[0]] <= current_node.condition[1]:
                current_node = current_node.left
            else:
                current_node = current_node.right

        # As soon as we are at a leaf, we classify x according to the majority class by rounding the average class
        average = sum([item[-1] for item in current_node.data]) / len(current_node.data)
        y.append(int(round(average)))

    return y


# For bagging (and random forests), you have to write two auxiliary func- tions called tree_grow_b and tree_pred_b. They are not much more than repeated applications of tree_grow and tree_pred respectively. The function tree_grow_b has all the arguments of tree_grow and in addition an argument m which denotes the number of bootstrap samples to be drawn. On each bootstrap sample a tree is grown. The function returns a list containing these m trees.

# In[10]:


def tree_grow_b(x, y, nmin, minleaf, nfeat, m):

    forest = []

    # Generate m trees
    for m in range(m):
        print('Bootstrap', m)
        forest.append(tree_grow(x, y, nmin, minleaf, nfeat))

    return forest



# Finally, the function tree_pred_b takes as input a list of trees and a data matrix x for which predictions are required. The function applies tree_pred to x using each tree in the list in turn. For each row of x the final prediction is obtained by taking the majority vote of the m predictions. The function returns a vector y, where y[i] contains the predicted class label for row i of x.

# In[11]:


def tree_pred_b(forest, x):

    votes = []

    # Get a vector of 'votes' for every tree
    for tree in forest:
        votes.append(tree_pred(x, tree))

    # Calculate the majority vote for each row in x
    for i in range(len(x)):
        all_votes_for_x = [vote[i] for vote in votes]
        majority_vote = int(round(sum(all_votes_for_x) / len(all_votes_for_x)))
        y.append(majority_vote)

    return y
